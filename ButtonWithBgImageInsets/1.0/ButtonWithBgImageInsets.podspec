Pod::Spec.new do |s|
  s.name         = "ButtonWithBgImageInsets"
  s.version      = "1.0"
  s.summary      = "ButtonWithBgImageInsets"
  s.homepage     = "https://bitbucket.org/polecat/specs.buttonwithbgimageinsets"
  s.license      = 'No license'
  s.author       = { "Dmitry Vorobyov" => "dvor@dvor.me" }
  s.platform     = :ios, '5.0'
  s.source       = { :git => "https://bitbucket.org/polecat/specs.buttonwithbgimageinsets.git", :tag => s.version.to_s}
  s.source_files = 'source'
  s.requires_arc = true

end
