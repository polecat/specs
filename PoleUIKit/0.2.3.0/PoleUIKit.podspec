Pod::Spec.new do |s|
  s.name         = 'PoleUIKit'
  s.version      = '0.2.3.0'
  s.summary      = 'PoleUIKit'
  s.homepage     = 'https://bitbucket.org/polecat/PoleUIKit'
  s.license      = 'No license'
  s.author       = { 'Sergey Korolkov' => 'skorolkov@ipolecat.com' }
  s.platform     = :ios, '8.0'
  s.source       = { :git => 'https://bitbucket.org/polecat/poleuikit.git', :tag => s.version.to_s}
  s.source_files = 'PoleUIKit/**/*.{h,m,swift}'
  s.requires_arc = true
  s.frameworks    = 'QuartzCore', 'UIKit'

end
