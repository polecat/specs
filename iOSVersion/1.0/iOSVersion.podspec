Pod::Spec.new do |s|
  s.name         = "iOSVersion"
  s.version      = "1.0"
  s.summary      = "Macro for getting iOS version"
  s.homepage     = "https://bitbucket.org/polecat/specs.iosversion"
  s.license      = 'No license'
  s.author       = { "Dmitry Vorobyov" => "dvor@dvor.me" }
  s.platform     = :ios, '5.0'
  s.source       = { :git => "https://bitbucket.org/polecat/specs.iosversion.git", :tag => s.version.to_s}
  s.source_files = 'source'
  s.requires_arc = true

end
