Pod::Spec.new do |s|
  s.name         = "AnimationChain"
  s.version      = "1.0"
  s.summary      = "AnimationChain"
  s.homepage     = "https://bitbucket.org/polecat/specs.animationchain"
  s.license      = 'No license'
  s.author       = { "Boris Zhdanov" => "bzhdanov@ipolecat.com" }
  s.platform     = :ios, '5.0'
  s.source       = { :git => "https://bitbucket.org/polecat/specs.animationchain.git", :tag => s.version.to_s}
  s.source_files = 'source'
  s.requires_arc = true

end
