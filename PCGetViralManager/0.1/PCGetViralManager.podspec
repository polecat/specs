Pod::Spec.new do |s|
  s.name         = "PCGetViralManager"
  s.version      = "0.1"
  s.summary      = "PCGetViralManager"
  s.homepage     = "https://bitbucket.org/polecat/specs.pcgetviralmanager"
  s.license      = 'No license'
  s.author       = { "Sergey Korolkov" => "skorolkov@ipolecat.com" }
  s.platform     = :ios, '7.0'
  s.source       = { :git => "https://bitbucket.org/polecat/specs.pcgetviralmanager.git", :tag => s.version.to_s}
  s.source_files = 'source'
  s.requires_arc = true
  s.framework    = "SystemConfiguration"

end
