Pod::Spec.new do |s|
  s.name         = 'WMRequest'
  s.version      = '1.0'
  s.summary      = 'WMRequest'
  s.homepage     = 'https://bitbucket.org/polecat/specs.wmrequest'
  s.license      = 'No license'
  s.author       = { 'Boris Zhdanov' => 'd503work@gmail.com' }
  s.platform     = :ios, '8.0'
  s.source       = { :git => 'https://bitbucket.org/polecat/specs.wmrequest.git', :tag => s.version.to_s}
  s.source_files = 'source/**/*.{h,m}'
  s.requires_arc = true
  s.dependency     'AFNetworking', '~> 2.6.0'
  s.dependency      'CocoaLumberjack', '~> 2.0.1'
end
