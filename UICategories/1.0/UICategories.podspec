Pod::Spec.new do |s|
  s.name         = "UICategories"
  s.version      = "1.0"
  s.summary      = "UICategories"
  s.homepage     = "https://bitbucket.org/polecat/specs.uicategories"
  s.license      = 'No license'
  s.author       = { "Dmitry Vorobyov" => "dvor@dvor.me" }
  s.platform     = :ios, '5.0'
  s.source       = { :git => "https://bitbucket.org/polecat/specs.uicategories.git", :tag => s.version.to_s}
  s.source_files = 'source'
  s.requires_arc = true
  s.framework    = "QuartzCore"

end
