Pod::Spec.new do |s|
  s.name         = 'UICategories-Swift-2_0'
  s.version      = '1.2.1'
  s.summary      = 'UICategories-Swift-2_0'
  s.homepage     = 'https://bitbucket.org/polecat/specs.uicategories'
  s.license      = 'No license'
  s.author       = { 'Dmitry Vorobyov' => 'dvor@dvor.me' }
  s.platform     = :ios, '8.0'
  s.source       = { :git => 'https://bitbucket.org/polecat/specs.uicategories.git', :tag => s.version.to_s}
  s.source_files = 'sourceSwift'
  s.requires_arc = true
  s.frameworks    = 'QuartzCore', 'UIKit'

end
